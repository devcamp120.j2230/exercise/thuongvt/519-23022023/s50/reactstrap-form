import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Form from './component/form';

function App() {
  return (
    <div className="container mt-5 text-center">
      <Form></Form>
    </div>
  );
}

export default App;
